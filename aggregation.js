db.fruits.insertMany ([
		{
			name: "Apple",
			color: "Red",
			stock: 20,
			price: 40,
			supplier_id : 1,
			onSale: true,
			origin: ["Philippines", "US"]
		},

		{
			name: "Banana",
			color: "Yellow",
			stock: 15,
			price: 20,
			supplier_id : 2,
			onSale: true,
			origin: ["Philippines", "Ecuador"]	
		},
		{
			name: "Kiwi",
			color: "Green",
			stock: 25,
			price: 50,
			supplier_id : 1,
			onSale: true,
			origin: ["US", "China"]
		},
			name: "Mango",
			color: "Yellow",
			stock: 10,
			price: 120,
			supplier_id : 2,
			onSale: false,
			origin: ["Philippines", "India"]

	]);

// MongoDB Aggregation
db.fruits.aggregate([

	// match is used to pass the documents that meet the specified condition/s to the next aggregation stage
	{ $match: {onSale: true} },

	// used to group elements together and field-value pairs using the data from the grouped elements
	{ $group: {_id: "$supplier_id", total: {$sum: "$stock"} } } 

	]);

	db.fruits.aggregate([
		{ $match: {onSale: true} },
		{ $group: {_id: "$supplier_id", total: {$sum: "$stock"} } },
		{ $project: { _id: 0 }}

	]);

	// sort - changes the order of the aggregated results
	db.fruits.aggregate([
		{ $match: {onSale: true} },
		{ $group: {_id: "$supplier_id", total: {$sum: "$stock"} } },
		{ $sort: {total: -1 }}
	]);


// aggregating results based on array fields

	// unwind
	db.fruits.aggregate([
		{ $unwind: "$origin"}
	]);

	db.fruits.aggregate([
		{ $unwind: "$origin"},
		{ $group: {_id: "$origin", kinds: { $sum:1 } } }
	]);

// other aggregate stages
	
	// count
	db.fruits.aggregate([
		{ $match: {color: "Yellow"} },
		{ $count: "Yellow fruits"}

	]);


	// average
	db.fruits.aggregate([
		{ $match: {color: "Yellow"} },
		{ $group: { _id: "$color", yellow_fruits_stock: {$avg: "$stock"} }}

	]);

	// $min and &max
	db.fruits.aggregate([
		{ $match: {color: "Yellow"} },
		{ $group: { _id: "$color", yellow_fruits_stock: {$min: "$stock"} }}

	]);

	db.fruits.aggregate([
		{ $match: {color: "Yellow"} },
		{ $group: { _id: "$color", yellow_fruits_stock: {$max: "$stock"} }}

	]);